
import { lazy } from "react";

let routes=[
    {
        path:'/home',
        component:lazy(()=>import('../views/home')),
        children:[
            {
                path:'/home/index',
                name:'首页',
                component:lazy(()=>import('../views/home/index'))
            },
            {
                path:'/home/classily',
                name:'分类',
                component:lazy(()=>import('../views/home/classily'))
            },
            {
                path:'/home/shopcar',
                name:'购物车',
                component:lazy(()=>import('../views/home/shopcar'))
            },
            {
                path:'/home/my',
                name:'我的',
                component:lazy(()=>import('../views/home/my'))
            },
        ]
    },
    {
        to:'/home',
        from:'/'
    }
]

export default routes