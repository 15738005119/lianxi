import React, { Component, Suspense } from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
import { Routes } from '../uiltes/types'
export default class routeView extends Component<Routes>{
    render() {
        let { routes } = this.props
        let list = routes.filter(item => !item.to)
        let redirect = routes.filter(item => item.to)[0]
        return (
            <Suspense fallback="Loading...">
                <Switch>
                    {
                        list && list.length > 0 ? list.map((item, index) => {
                            return <Route
                                key={index}
                                path={item.path}
                                render={ref => {
                                    let Com = item.component

                                    if (item.children) {
                                        return <Com
                                            {...ref}
                                            routes={item.children}
                                            navlink={item.children.filter(item => !item.to)}
                                        />
                                    }

                                    return <Com {...ref} />
                                }}
                            />
                        }) : <p>暂无数据</p>
                    }
                    {
                        redirect && <Redirect to={redirect.to as string} from={redirect.from}></Redirect>
                    }
                </Switch>
            </Suspense>
        )
    }
}
