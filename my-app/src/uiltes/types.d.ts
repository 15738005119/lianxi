
export interface Routes{
    routes:Array<RoutesItem>,
    navlink?:Array<RoutesItem>
}

export interface RoutesItem{
    path?:string,
    name?:string,
    component?:any,
    to?:string,
    from?:string,
    children?:RoutesItem[]
}