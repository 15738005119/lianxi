import React, { Component } from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import RouteView from './route/routeView'
import routes from './route/routeList'
export default class App extends Component {
  render() {
    return (
      <Router>
          <RouteView routes={routes}></RouteView>
      </Router>
    )
  }
}
