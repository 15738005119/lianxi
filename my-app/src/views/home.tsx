import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import RouteView from '../route/routeView'
import {Routes} from '../uiltes/types'
import './scss/home.css'
export default class home extends Component<Routes>{
  render() {
      let {navlink,routes} = this.props
    return (
      <div className='home'>
          <div className='cont'>
              <RouteView routes={routes}></RouteView>
          </div>
          <div className='foot'>
            {
                navlink && navlink.length>0 ? navlink.map((item,index)=>{
                    return <NavLink to={item.path as string} key={index}>{item.name}</NavLink>
                }):<p>暂无数据</p>
            }
          </div>
      </div>
    )
  }
}
